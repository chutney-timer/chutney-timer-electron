# chutney-timer

> A no shit pomodoro timer

## ChangeLog

- **0.1.1**: Config Directory creation bug fixed.
- **0.1.0**: Not limited to a long-break anymore. Seperate record button.
- **0.0.4**: Added Task name input on timer page.
- **0.0.3**: Added timer settings options and auto sync.
- **0.0.1 and 0.0.2**: Very early build.

#### Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build
```
