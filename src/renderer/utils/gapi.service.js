import { parse } from "url";
import { remote } from "electron";
import axios from "axios";
import qs from "qs";
import { google } from "googleapis";

const GOOGLE_AUTHORIZATION_URL = "https://accounts.google.com/o/oauth2/v2/auth";
const GOOGLE_TOKEN_URL = "https://www.googleapis.com/oauth2/v4/token";
const GOOGLE_PROFILE_URL = "https://www.googleapis.com/userinfo/v2/me";
export const GOOGLE_DRIVE_URL = "https://www.googleapis.com/drive/v2/files";
const CLIENT_ID =
  "552686179446-3n0a9kg86c38d7nvngdppmd6q1ihj34s.apps.googleusercontent.com";
const CLIENT_SECRET = "g54YvJzwdMDgYBr7_rK2NID_";
const GOOGLE_REDIRECT_URI = "http://localhost:9080";

export const apiConfig = {
  apiKey: "AIzaSyAt-Ja-lYsgPAMw1ZGvcIPYuYmQT-Juv28",
  clientId: CLIENT_ID,
  discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"],
  scope: "https://www.googleapis.com/auth/drive.appdata"
};

export function getOauthClient() {
  return new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, GOOGLE_REDIRECT_URI);
}

export async function googleSignIn(resolve) {
  const code = await signInWithPopup();
  const tokens = await fetchAccessTokens(code);
  const { id, email, name } = await fetchGoogleProfile(tokens.access_token);
  const timeStamp = new Date().toUTCString();
  const providerUser = {
    uid: id,
    email,
    displayName: name,
    idToken: tokens,
    timeStamp
  };

  return resolve(providerUser);
}
export function signInWithPopup() {
  return new Promise((resolve, reject) => {
    const authWindow = new remote.BrowserWindow({
      width: 500,
      height: 600,
      show: true
    });

    // TODO: Generate and validate PKCE code_challenge value
    const urlParams = {
      access_type: "offline",
      response_type: "code",
      redirect_uri: GOOGLE_REDIRECT_URI,
      client_id: CLIENT_ID,
      scope: "https://www.googleapis.com/auth/drive.appdata profile email"
    };
    const authUrl = `${GOOGLE_AUTHORIZATION_URL}?${qs.stringify(urlParams)}`;

    function handleNavigation(url) {
      const query = parse(url, true).query;
      if (query) {
        if (query.error) {
          reject(new Error(`There was an error: ${query.error}`));
        } else if (query.code) {
          // Login is complete
          authWindow.removeAllListeners("closed");
          setImmediate(() => authWindow.close());

          // This is the authorization code we need to request tokens
          resolve(query.code);
        }
      }
    }

    authWindow.on("closed", () => {
      // TODO: Handle this smoothly
      throw new Error("Auth window was closed by user");
    });

    authWindow.webContents.on("will-navigate", (event, url) => {
      handleNavigation(url);
    });

    authWindow.webContents.on(
      "did-get-redirect-request",
      (event, oldUrl, newUrl) => {
        handleNavigation(newUrl);
      }
    );

    authWindow.loadURL(authUrl);
  });
}

export async function fetchAccessTokens(code) {
  const response = await axios.post(
    GOOGLE_TOKEN_URL,
    qs.stringify({
      code,
      client_id: CLIENT_ID,
      redirect_uri: GOOGLE_REDIRECT_URI,
      grant_type: "authorization_code",
      client_secret: CLIENT_SECRET
    }),
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }
  );
  return response.data;
}

export async function refreshAccessTokens(code) {
  const response = await axios.post(
    GOOGLE_TOKEN_URL,
    qs.stringify({
      refresh_token: code,
      client_id: CLIENT_ID,
      grant_type: "refresh_token",
      client_secret: CLIENT_SECRET
    }),
    {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }
  );
  return response.data;
}

export async function fetchGoogleProfile(accessToken) {
  const response = await axios.get(GOOGLE_PROFILE_URL, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`
    }
  });
  return response.data;
}
