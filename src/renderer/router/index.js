import Vue from "vue";
import Router from "vue-router";
import VueGAPI from "vue-gapi";

Vue.use(Router);
export default new Router({
  routes: [
    {
      path: "/settings",
      name: "settings-view",
      component: require("@/components/SettingsView").default
    },
    {
      path: "/",
      name: "timer-view",
      component: require("@/components/TimerView").default
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
