import {
  ADD_SESSION,
  SET_SYNC_DATA_MUTATION,
  CLEAR_DATA_MUTATION
} from "./mutations.type";
import {
  ADD_SESSION_ENTRY,
  SYNC_DATA,
  CLEAR_DATA,
  REFRESH_TOKEN
} from "./actions.type";
import { google } from "googleapis";
import fs from "fs";
import path from "path";
import os from "os";
import uuid from "uuid";
import { getOauthClient } from "../utils/gapi.service";

const state = {
  timeLogs: {}
};

const getters = {
  allTimeLogs(state) {
    console.log(state, state.timeLogs);
    return state.timeLogs;
  }
};

const actions = {
  [CLEAR_DATA](context) {
    context.commit(CLEAR_DATA_MUTATION);
  },
  [ADD_SESSION_ENTRY](context, data) {
    console.log("sd", data);
    data.startTime = data.startTime.toUTCString();
    let st = data.startTime;
    console.log(st, data);
    context.commit(ADD_SESSION, { key: st, data: data });
    context.dispatch(SYNC_DATA);
  },
  [SYNC_DATA](context) {
    let data = context.state.timeLogs;
    context.dispatch(`gauth/${REFRESH_TOKEN}`, { root: true });
    let authData = context.rootState.gauth.authData;
    if (Object.keys(authData).length) {
      const filePath = path.join(os.tmpdir(), uuid.v4());

      function uploadData(id) {
        console.log(data);
        fs.writeFile(filePath, JSON.stringify(data), function(err) {
          if (err) {
            return console.log(err);
          }
          var fileMetadata = {
            name: "logs.json"
          };
          var media = {
            mimeType: "application/json",
            body: fs.createReadStream(filePath)
          };
          if (id) {
            drive.files.update(
              {
                fileId: id,
                resource: fileMetadata,
                media: media,
                fields: "id"
              },
              function(err, file) {
                if (err) {
                  // Handle error
                  console.error(err);
                } else {
                  console.log("File Id:", file);
                  context.commit(SET_SYNC_DATA_MUTATION, data);
                }
              }
            );
          } else {
            fileMetadata.parents = ["appDataFolder"];
            drive.files.create(
              {
                resource: fileMetadata,
                media: media,
                fields: "id"
              },
              function(err, file) {
                if (err) {
                  // Handle error
                  console.error(err);
                } else {
                  console.log("File Id:", file);
                  context.commit(SET_SYNC_DATA_MUTATION, data);
                }
              }
            );
          }
        });
      }

      let authClient = getOauthClient();
      authClient.setCredentials(authData.idToken);
      let drive = google.drive({
        version: "v3",
        auth: authClient
      });
      drive.files.list(
        {
          spaces: "appDataFolder",
          fields: "nextPageToken, files(id, name)",
          pageSize: 100
        },
        async function(err, res) {
          if (err) {
            // Handle error
            console.error(err);
          } else {
            if (res.data.files.length > 0) {
              console.log(res.data);
              let file = res.data.files[0];
              var getFile = await drive.files.get({
                fileId: file.id,
                alt: "media"
              });
              console.log("before", data);
              console.log("upcoming", getFile);
              Object.keys(getFile.data).map(key => {
                console.log("Data", getFile.data[key]);
                context.commit(ADD_SESSION, getFile.data[key]);
              });
              console.log("now", data);
              uploadData(file.id);
            } else {
              uploadData();
            }
          }
        }
      );
    }
  }
};

const mutations = {
  [SET_SYNC_DATA_MUTATION](state, data) {
    state.timeLogs = data;
  },
  [ADD_SESSION](state, data) {
    state.timeLogs[data.key] = data.data;
  },
  [CLEAR_DATA_MUTATION](state) {
    state.timeLogs = {};
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
