import {
  MUTATE_SPRINT_TIME,
  UPDATE_LBREAK,
  UPDATE_SBREAK,
  UPDATE_SPRINTS,
  UPDATE_TIME
} from "./mutations.type";
import { CHANGE_SPRINT_TIME } from "./actions.type";

const DEFAULT_SETTINGS = {
  timer: {
    sprints: 4,
    time: 25,
    sBreak: 5,
    lBreak: 15
  }
};

const state = {
  settings: JSON.parse(JSON.stringify(DEFAULT_SETTINGS))
};

const getters = {
  timerSettings(state) {
    return state.settings.timer;
  }
};

const actions = {
  [CHANGE_SPRINT_TIME](context, data) {
    context.commit(MUTATE_SPRINT_TIME, data);
  }
};

const mutations = {
  [MUTATE_SPRINT_TIME](state, data) {
    state.settings.timer = parseInt(data);
  },
  [UPDATE_LBREAK](state, data) {
    state.settings.timer.lBreak = parseInt(data);
  },
  [UPDATE_SBREAK](state, data) {
    state.settings.timer.sBreak = parseInt(data);
  },
  [UPDATE_TIME](state, data) {
    state.settings.timer.time = parseInt(data);
  },
  [UPDATE_SPRINTS](state, data) {
    state.settings.timer.sprints = parseInt(data);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
