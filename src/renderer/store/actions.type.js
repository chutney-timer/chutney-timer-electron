export const ADD_SESSION_ENTRY = "addSessionEntry";
export const SET_SYNC_DATA = "setSyncData";
export const SYNC_DATA = "syncData";
export const CLEAR_DATA = "clearData";
export const CHANGE_SPRINT_TIME = "changeSprintTime";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REFRESH_TOKEN = "refreshToken";
