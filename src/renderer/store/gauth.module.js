import {
  LOGIN_MUTATE,
  REFRESH_TOKEN_MUTATION,
  LOGOUT_MUTATE
} from "./mutations.type";
import { LOGIN, REFRESH_TOKEN, LOGOUT } from "./actions.type";
import { googleSignIn, refreshAccessTokens } from "../utils/gapi.service";

const state = {
  authData: {}
};

const getters = {
  authData(state) {
    return state.authData;
  }
};

const actions = {
  [LOGIN](context) {
    googleSignIn(function(data) {
      context.commit(LOGIN_MUTATE, data);
    });
  },
  [LOGOUT](context) {
    context.commit(LOGIN_MUTATE);
  },
  [REFRESH_TOKEN](context) {
    if (Object.keys(context.state.authData).length) {
      let tokenData = context.state.authData.idToken;
      let now = new Date();
      let timeStamp = new Date(context.state.authData.timeStamp);
      console.log(timeStamp);
      var diff = (now.getTime() - timeStamp.getTime()) / 1000;
      if (diff > tokenData.expires_in - 200) {
        let newData = refreshAccessTokens(tokenData.refresh_token);
        console.log(newData);
        if (newData.access_token) {
          context.commit(REFRESH_TOKEN_MUTATION, newData);
        }
      }
    }
  }
};

const mutations = {
  [LOGIN_MUTATE](state, data) {
    state.authData = data;
  },
  [LOGOUT_MUTATE](state) {
    state.authData = {};
  },
  [REFRESH_TOKEN_MUTATION](state, data) {
    let stateData = state.authData;
    Object.keys(data).map(key => {
      stateData[key] = data[key];
    });
    sate.authData = stateData;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
