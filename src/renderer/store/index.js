import Vue from "vue";
import Vuex from "vuex";
import timelogs from "./timelog.module";
import settings from "./settings.module";
import gauth from "./gauth.module";
import fs from "fs";
import path from "path";
//import { createPersistedState, createSharedMutations } from "vuex-electron";
import VuexPersist from "vuex-persistfile";
const remote = require("electron").remote;
const app = remote.app;

export const userDataDir = path.join(app.getPath("userData"), "data");

fs.mkdir(userDataDir, function(err) {
  if (err) {
    return console.error(err);
  }
  console.log("Directory created successfully!");
});

const persist = new VuexPersist({
  path: userDataDir
});

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    timelogs,
    settings,
    gauth
  },
  plugins: [persist.subscribe()],
  //plugins: [createPersistedState(), createSharedMutations()]
  strict: process.env.NODE_ENV !== "production"
});
